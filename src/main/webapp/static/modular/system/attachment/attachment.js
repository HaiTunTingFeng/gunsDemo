/**
 * 附件管理管理初始化
 */
var Attachment = {
    queryData: {
        limit: 10,      		    //每页的记录行数（*）
        offset: 0,      			//当前已查询条数
        type: '',      			    //文件查询类型
        label: ''         			//文件标签
    },
    layerIndex: -1
};

/**
 * 查询列表
 */
Attachment.search = function () {
    $('#fileView').empty();
    Attachment.pageContent();
};

/**
 *  type控制
 */
Attachment.type = function (id) {
    $('.file-control').attr('class', 'file-control');
    $('#type_' + id).attr('class', 'file-control active');
    this.queryData.type = id;
    Attachment.queryData.offset = 0;
    Attachment.search();
};

/**
 *  label控制
 */
Attachment.label = function (label) {
    this.queryData.label = label;
    Attachment.queryData.offset = 0;
    Attachment.search();
};

/**
 * 查询文件分页信息
 */
Attachment.pageContent = function () {
    var loadIndex = parent.layer.load(1, {
        shade: [0.1, '#fff']
    });
    var ajax = new $ax(Feng.ctxPath + "/attachment/list", function (data) {
        if (data.rows.length == 0) {
            Feng.alert('没有更多数据！', 1);
            $("#PageNext").hide();
        } else if (Attachment.queryData.offset == data.total) {
            Attachment.pageView(data.rows);
            Feng.alert('所有数据已加载完成！', 1);
            $("#PageNext").hide();
        } else {
            Attachment.pageView(data.rows);
            $("#PageNext").show();
        }
        parent.layer.close(loadIndex);
    }, function (data) {
        Feng.error("文件信息查询失败!" + data.message + "!");
        parent.layer.close(loadIndex);
    });
    ajax.set(Attachment.queryData);
    ajax.start();
};

/**
 *  文件展示渲染
 */
Attachment.pageView = function (files) {
    var fileHtml = new Array();
    files.forEach(function (file) {
        var fileName = file.name.length > 25 ? file.name.substring(0, 20) + "..." : file.name;
        fileHtml.push(
            '<div class="file-box">' +
            '    <div class="file">' +
            '        <a href="javaScript:void(0)">' +
            '            <span class="corner"></span>' +
            // '            <div class="image"><img alt="image" class="img-responsive" src="' + Feng.ctxPath + file.path + '"></div>' +
            '            <div class="icon">' +
            '                <i class="fa fa-file"></i>' +
            '            </div>' +
            '            <div class="file-name"><div>' + fileName + '</div><br/>' + '<small>添加时间：' + file.createDate + '</small></div>' +
            '        </a>' +
            '    </div>' +
            '</div>')
    });
    Attachment.queryData['offset'] = Feng.accAdd(Attachment.queryData.offset, files.length);
    $('#fileView').append(fileHtml.join(''));
};

/**
 *  标签渲染
 *
 */
Attachment.labelView = function () {
    var laelList = 'all,爱人,工作,家庭,孩子,假期,音乐,照片,电影';
    var labelHtml = new Array();
    laelList.split(',').forEach(function (label) {
        labelHtml.push(
            '<li onclick="Attachment.label(\'' + label + '\')"><a href="javaScript:void(0)">' + label + '</a></li>')
    });
    $('#label').append(labelHtml.join(''));
};

/**
 * 上传控件初始化
 */
Attachment.initUpload = function () {
    $("#file").fileinput({
        language: 'zh', //设置语言
        uploadUrl: Feng.ctxPath + "/attachment/uploadFile", //上传的地址
        //uploadExtraData:{"id": 1, "fileName":'123.mp3'},
        uploadAsync: false, //默认异步上传
        showUpload: true, //是否显示上传按钮
        showRemove: true, //显示移除按钮
        showCancel: true, //显示移除按钮
        showPreview: true, //是否显示预览
        showCaption: false,//是否显示标题
        browseClass: "btn btn-primary", //按钮样式
        dropZoneEnabled: true,//是否显示拖拽区域
        // minImageWidth: 50, //图片的最小宽度
        // minImageHeight: 50,//图片的最小高度
        // maxImageWidth: 1000,//图片的最大宽度
        // maxImageHeight: 1000,//图片的最大高度
        maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
        //minFileCount: 0,
        maxFileCount: 10, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount: true,
        previewFileIcon: "<iclass='glyphicon glyphicon-king'></i>",
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
    }).on("fileuploaded", function (event, data, previewId, index) {

    }).on("filecleared", function (event, data, msg) {
        $("#fileInput").hide();
    });
};

$(function () {
    //获取初始页面数据
    Attachment.pageContent();
    //标签渲染
    Attachment.labelView();
    //初始化上传控件
    Attachment.initUpload();
    //数据加载
    $("#PageNext").click(function () {
        Attachment.pageContent();
    });
    //展示隐藏上传控件
    $("#showFile").click(function () {
        $("#fileInput").fadeToggle(1000);
    });
    $("#fileInput").hide();
});
