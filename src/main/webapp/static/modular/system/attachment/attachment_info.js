/**
 * 初始化附件管理详情对话框
 */
var AttachmentInfoDlg = {
    attachmentInfoData : {}
};

/**
 * 清除数据
 */
AttachmentInfoDlg.clearData = function() {
    this.attachmentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AttachmentInfoDlg.set = function(key, val) {
    this.attachmentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 获取对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AttachmentInfoDlg.get = function(key) {
    return Judge.getValue($("#" + key).val());
}

/**
 * 关闭此对话框
 */
AttachmentInfoDlg.close = function() {
    parent.layer.close(window.parent.Attachment.layerIndex);
}

/**
 * 收集数据
 */
AttachmentInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('extName')
    .set('path')
    .set('createBy')
    .set('createDate')
    .set('updateBy')
    .set('updateDate')
    .set('isActive');
}

/**
 * 提交添加
 */
AttachmentInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/attachment/add", function(data){
        if(data.code == 200){
            Feng.success(data.message);
            window.parent.Attachment.table.refresh();
            AttachmentInfoDlg.close();
        }else{
            Feng.success(data.message);
        }
    },function(data){
        Feng.error("添加失败!" + data.message + "!");
    });
    ajax.set(this.attachmentInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AttachmentInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/attachment/update", function(data){
        if(data.code == 200){
            Feng.success(data.message);
            window.parent.Attachment.table.refresh();
            AttachmentInfoDlg.close();
        }else{
            Feng.success(data.message);
        }
    },function(data){
        Feng.error("修改失败!" + data.message + "!");
    });
    ajax.set(this.attachmentInfoData);
    ajax.start();
}

$(function() {

});
