var Judge = {
    isNull: function (str) {
        var key;
        if (str === "" || str === null || str === false || str === 'undefined') {
            return true;
        }
        if (typeof str == 'object') {
            for (key in str) {
                return false;
            }
            return true;
        }
        return false;
    },
    isNotNull: function (str) {
        return !this.isNull(str);
    },
    isType: function (boj, type) {
        //判断数据是否是固定类型 （type 首字母大写）
        return Object.prototype.toString.call(boj) == '[object ' + type + ']';
    },
    getValue: function (str, defaultStr) {
        return this.isNull(str) ? (this.isNull(defaultStr) ? '' : defaultStr) : str;
    }
};