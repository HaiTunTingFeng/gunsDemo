package cn.stylefeng.guns.generator.engine.config;


import cn.hutool.core.util.StrUtil;

/**
 * 全局配置
 *
 * @author fengshuonan
 * @date 2017-05-08 20:21
 */
public class ContextConfig {

    private String templatePrefixPath = "gunsTemplate/advanced";
    /**
     * 模板输出的项目目录
     */
    private String projectPath = "D:\\ideaSpace\\guns";
    /**
     * 业务名称
     */
    private String bizChName;
    /**
     * 业务英文名称
     */
    private String bizEnName;
    /**
     * 业务英文名称(大写)
     */
    private String bizEnBigName;
    /**
     * 模块名称
     */
    private String moduleName = "system";

    private String proPackage = "cn.stylefeng.guns.admin";
    private String coreBasePackage = "cn.stylefeng.guns.core";
    private String modelPackageName = "cn.stylefeng.guns.modular.system.model";
    private String modelMapperPackageName = "cn.stylefeng.guns.modular.system.dao";
    /**
     * 实体的名称
     */
    private String entityName;

    /**
     * controllerSwitch :是否生成控制器代码开关
     * controllerSwitch :主页
     * controllerSwitch :添加页面
     * controllerSwitch :编辑页面
     * controllerSwitch :js
     * controllerSwitch :详情页面js
     * controllerSwitch :dao
     * controllerSwitch :service
     * controllerSwitch :生成实体的开关
     * controllerSwitch :生成sql的开关
     */
    private Boolean controllerSwitch = true;
    private Boolean indexPageSwitch = true;
    private Boolean addPageSwitch = true;
    private Boolean editPageSwitch = true;
    private Boolean jsSwitch = true;
    private Boolean infoJsSwitch = true;
    private Boolean daoSwitch = true;
    private Boolean serviceSwitch = true;
    private Boolean entitySwitch = true;
    private Boolean sqlSwitch = true;

    public void init() {
        if (entityName == null) {
            entityName = bizEnBigName;
        }
        modelPackageName = proPackage + "." + "modular.system.model";
        modelMapperPackageName = proPackage + "." + "modular.system.dao";
    }

    public String getBizEnBigName() {
        return bizEnBigName;
    }

    public void setBizEnBigName(String bizEnBigName) {
        this.bizEnBigName = bizEnBigName;
    }

    public String getBizChName() {
        return bizChName;
    }

    public void setBizChName(String bizChName) {
        this.bizChName = bizChName;
    }

    public String getBizEnName() {
        return bizEnName;
    }

    public void setBizEnName(String bizEnName) {
        this.bizEnName = bizEnName;
        this.bizEnBigName = StrUtil.upperFirst(this.bizEnName);
    }

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Boolean getControllerSwitch() {
        return controllerSwitch;
    }

    public void setControllerSwitch(Boolean controllerSwitch) {
        this.controllerSwitch = controllerSwitch;
    }

    public Boolean getIndexPageSwitch() {
        return indexPageSwitch;
    }

    public void setIndexPageSwitch(Boolean indexPageSwitch) {
        this.indexPageSwitch = indexPageSwitch;
    }

    public Boolean getAddPageSwitch() {
        return addPageSwitch;
    }

    public void setAddPageSwitch(Boolean addPageSwitch) {
        this.addPageSwitch = addPageSwitch;
    }

    public Boolean getEditPageSwitch() {
        return editPageSwitch;
    }

    public void setEditPageSwitch(Boolean editPageSwitch) {
        this.editPageSwitch = editPageSwitch;
    }

    public Boolean getJsSwitch() {
        return jsSwitch;
    }

    public void setJsSwitch(Boolean jsSwitch) {
        this.jsSwitch = jsSwitch;
    }

    public Boolean getInfoJsSwitch() {
        return infoJsSwitch;
    }

    public void setInfoJsSwitch(Boolean infoJsSwitch) {
        this.infoJsSwitch = infoJsSwitch;
    }

    public Boolean getDaoSwitch() {
        return daoSwitch;
    }

    public void setDaoSwitch(Boolean daoSwitch) {
        this.daoSwitch = daoSwitch;
    }

    public Boolean getServiceSwitch() {
        return serviceSwitch;
    }

    public void setServiceSwitch(Boolean serviceSwitch) {
        this.serviceSwitch = serviceSwitch;
    }

    public String getTemplatePrefixPath() {
        return templatePrefixPath;
    }

    public void setTemplatePrefixPath(String templatePrefixPath) {
        this.templatePrefixPath = templatePrefixPath;
    }

    public String getModelPackageName() {
        return modelPackageName;
    }

    public void setModelPackageName(String modelPackageName) {
        this.modelPackageName = modelPackageName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getModelMapperPackageName() {
        return modelMapperPackageName;
    }

    public void setModelMapperPackageName(String modelMapperPackageName) {
        this.modelMapperPackageName = modelMapperPackageName;
    }

    public Boolean getEntitySwitch() {
        return entitySwitch;
    }

    public void setEntitySwitch(Boolean entitySwitch) {
        this.entitySwitch = entitySwitch;
    }

    public Boolean getSqlSwitch() {
        return sqlSwitch;
    }

    public void setSqlSwitch(Boolean sqlSwitch) {
        this.sqlSwitch = sqlSwitch;
    }

    public String getProPackage() {
        return proPackage;
    }

    public void setProPackage(String proPackage) {
        this.proPackage = proPackage;
    }

    public String getCoreBasePackage() {
        return coreBasePackage;
    }

    public void setCoreBasePackage(String coreBasePackage) {
        this.coreBasePackage = coreBasePackage;
    }
}
