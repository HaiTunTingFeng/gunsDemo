package cn.stylefeng.guns.core.common.constant;

/**
 * 常用全局变量
 *
 * @author fengshuonan
 * @date 2017-08-23 9:23
 */
public interface ToolVariable {

    /**
     * 附件
     */
    String All = "all";

    String FILE = "txt,docx,doc,wps,xlsx,vsdx";

    String VIDEO = "AVI,WMV,RM,RMVB,MPEG1,MPEG2,MPEG4(MP4),3GP,ASF,SWF,VOB,DAT,MOV,M4V,FLV,F4V,MKV,MTS,TS";

    String PIC = "bmp,jpg,png,tif,gif,pcx,tga,exif,fpx,svg,psd,cdr,pcd,dxf,ufo,eps,ai,raw,WMF,webp";


}
