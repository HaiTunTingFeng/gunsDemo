package cn.stylefeng.guns.core.util;

import cn.stylefeng.guns.core.common.constant.ToolVariable;
import cn.stylefeng.roses.core.util.ToolUtil;

public class ToolUtils extends ToolUtil {

    /**
     * 根据具体文件后缀判断文件类型
     *
     * @param fileExtName 文件后缀
     * @return
     */
    public static String fileFormat(String fileExtName) {
        if (ToolVariable.FILE.contains(fileExtName)) {
            return "file";
        } else if (ToolVariable.PIC.contains(fileExtName)) {
            return "pic";
        } else if (ToolVariable.VIDEO.contains(fileExtName)) {
            return "video";
        } else {
            return "Unknown";
        }
    }
}
