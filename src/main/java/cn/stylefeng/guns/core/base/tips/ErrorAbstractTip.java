package cn.stylefeng.guns.core.base.tips;

/**
 * 返回给前台的错误提示
 *
 * @author savor
 * @date 2016年11月12日 下午5:05:22
 */
public class ErrorAbstractTip extends AbstractTip {

    public ErrorAbstractTip() {
        super.code = 500;
        super.status = "N";
        super.message = "操作失败";
    }

    public ErrorAbstractTip(int code, String status, String message) {
        super();
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public ErrorAbstractTip(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }
}
