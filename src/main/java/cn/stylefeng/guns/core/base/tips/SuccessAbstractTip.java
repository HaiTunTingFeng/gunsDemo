package cn.stylefeng.guns.core.base.tips;

/**
 * 返回给前台的成功提示
 *
 * @author savor
 * @date 2016年11月12日 下午5:05:22
 */
public class SuccessAbstractTip extends AbstractTip {

    public SuccessAbstractTip() {
        super.code = 200;
        super.status = "Y";
        super.message = "操作成功";
    }

    public SuccessAbstractTip(int code, String status, String message) {
        super();
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public SuccessAbstractTip(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }
}
