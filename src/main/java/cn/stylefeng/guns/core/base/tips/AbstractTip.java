package cn.stylefeng.guns.core.base.tips;

/**
 * 返回给前台的提示（最终转化为json形式）
 *
 * @author savor
 * @Date 2017年1月11日 下午11:58:00
 */
public abstract class AbstractTip {

    /**
     * code:状态码
     * status:状态标识
     * message:信息内容
     */
    protected int code;
    protected String status;
    protected String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
