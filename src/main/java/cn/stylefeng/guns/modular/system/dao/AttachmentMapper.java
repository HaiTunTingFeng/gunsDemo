package cn.stylefeng.guns.modular.system.dao;

import cn.stylefeng.guns.modular.system.model.Attachment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2018-12-10
 */
public interface AttachmentMapper extends BaseMapper<Attachment> {

}
