package cn.stylefeng.guns.modular.system.service;

import cn.stylefeng.guns.modular.system.model.Attachment;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2018-12-10
 */
public interface IAttachmentService extends IService<Attachment> {

}
